from typing import Sequence
import pytorch_lightning as pl
from elektronn3.data import transforms
from knossos_data import KnossosRawDataSynthetic
import torch
from knossos_data_j0126 import KnossosRawData
from knossos_real_segm import KnossosRawDataSegm
from pathlib import Path

def get_transforms(divide=1):
    # Transformations to be applied to samples before feeding them to the network

    common_transforms = [
        # transforms.SqueezeTarget(dim=None),  # Workaround for neuro_data_cdhw
        # transforms.Lambda(lambda x, y: (x / divide, y))
    ]
    
    active_transforms = [ 
                        #  transforms.RandomRotate2d(prob=0.6),
                        #  transforms.RandomFlip(ndim_spatial=3),
                         transforms.RandomGrayAugment(channels=[0], prob=0.4),
                         transforms.RandomGammaCorrection(gamma_std=0.25, gamma_min=0.25, prob=0.5),
                        #  transforms.RandomGaussianBlur(channels=[0], prob=0.5),
                         transforms.AdditiveGaussianNoise(sigma=0.4, channels=[0], prob=0.6) #hard: sigma=0.2 und prob=0.7
                        #  transforms.ElasticTransform(channels=[0], prob=0.7)
                        ]
    
    train_transform = transforms.Compose(active_transforms + common_transforms)
    valid_transform = transforms.Compose(common_transforms)

    return train_transform, valid_transform


class UNETDataConfig:
    def __init__(self,
                 data_path_x: str,
                 ground_truth_x: str,
                 data_path_y:str,
                 ground_truth_y: str,
                 data_path_z: str,
                 ground_truth_z: str,
                 data_path_z2: str,
                 ground_truth_z2: str,
                 train_bounds: Sequence[Sequence[int]],
                 val_bounds: Sequence[Sequence[int]],
                 batch_size=2,
                 batch_size_logging=2,
                 epoch_size_train=500,
                 epoch_size_val=20,
                 num_workers=5,
                 patch_shape=(88, 88, 88),
                 patch_shape_logging=(64, 150, 150),
                 verbose=False,
                 **kwargs
                 ):
        """
        The configuration for the SECGAN Data
        :param batch_size: The batch size for training
        :param batch_size_logging: The batch size for logging
        :param epoch_size_train: The training epoch size
        :param epoch_size_val: The validation epoch size
        :param num_workers: The number of workers per dataloader
        :param patch_shape: The patch shape for training/evaluation
        :param patch_shape_logging: The patch shape for logging
        :param train_bounds: The bounds of the training patches for Y (in case GT is available)
        :param valid_bounds: The bounds of the validation patches for Y (in case GT is available)
        :param verbose: Whether to be verbose
        """
        self.data_path_x = data_path_x
        self.ground_truth_x = ground_truth_x
        self.data_path_y = data_path_y
        self.ground_truth_y = ground_truth_y
        self.data_path_z = data_path_z
        self.ground_truth_z = ground_truth_z
        self.ground_truth_z2 = ground_truth_z2
        self.data_path_z2 = data_path_z2
        self.train_bounds = train_bounds
        self.val_bounds = val_bounds
        self.batch_size = batch_size
        self.batch_size_logging = batch_size_logging
        self.epoch_size_train = epoch_size_train
        self.epoch_size_val = epoch_size_val
        self.num_workers = num_workers
        self.patch_shape = patch_shape
        self.patch_shape_logging = patch_shape_logging
        self.verbose = verbose

    
def tuple2bounds(coordinate_list):
    return coordinate_list[:3], coordinate_list[3:]

class ConcatDataset(torch.utils.data.Dataset):
    # https://discuss.pytorch.org/t/train-simultaneously-on-two-datasets/649/2

    def __init__(self, *datasets):
        self.datasets = datasets

    def __getitem__(self, i):
        return tuple(d[i] for d in self.datasets)

    def __len__(self):
        return min(len(d) for d in self.datasets)

def setup_data(args: UNETDataConfig):
    train_transform, valid_transform = get_transforms(divide=255)

    print('Loading datasets (this might take a while)')
    
    # train_dataset= KnossosRawDataSynthetic(data_path_x=args.data_path_x,
    #                                        gt_path_x=args.ground_truth_x,
    #                                        data_path_y=None,
    #                                        gt_path_y=None,
    #                                        data_path_z=None,
    #                                        gt_path_z=None, 
    #                                        patch_shape=args.patch_shape,  # [z]yx
    #                                        transform=train_transform,
    #                                        bounds=tuple2bounds(args.train_bounds),  # xyz
    #                                        epoch_size=args.epoch_size_train,
    #                                        disable_memory_check=False,
    #                                        verbose=args.verbose)
    
    # valid_dataset = KnossosRawDataSynthetic(data_path_x=args.data_path_y,
    #                                         gt_path_x=args.ground_truth_y,
    #                                         data_path_y=None,
    #                                         gt_path_y=None,
    #                                         data_path_z=None,
    #                                         gt_path_z=None, 
    #                                         # patch_shape=[30, 30, 30],  # [z]yx
    #                                         patch_shape=args.patch_shape,
    #                                         transform=valid_transform,
    #                                         bounds=tuple2bounds(args.val_bounds),  # xyz
    #                                         epoch_size=args.epoch_size_val,
    #                                         disable_memory_check=False,
    #                                         verbose=args.verbose)
    
    
    # data_path_train = Path(args.data_path_x)
    # raw_files_train = list(data_path_train.iterdir())
    # raw_h5 = [f for f in raw_files_train if str(f).endswith(".h5")]
    # raw_h5_train = raw_h5 
    
    # gt_path_train = Path(args.ground_truth_x)
    # unet_labels_train = list(gt_path_train.iterdir())
    # u_labels_train = unet_labels_train
    # print(f"len train = {len(u_labels_train)}")
    
    # data_path_val = Path(args.data_path_y)
    # raw_files_val = list(data_path_val.iterdir())
    # raw_h5 = [f for f in raw_files_val if str(f).endswith(".h5")]
    # raw_h5_val = raw_h5
    # print(raw_h5_val)
    
    # gt_path_val = Path(args.ground_truth_y)
    # unet_labels_val = list(gt_path_val.iterdir())
    # u_labels_val = unet_labels_val
    
    # p = Path("/cajal/nvmescratch/users/riegerfr/miki_seg/")
    # # p2 = Path("/cajal/scratch/users/anaml/GT/labels/all/")
    # p2 = Path("/cajal/scratch/users/anaml/diff_real_seg/")
    # raw_files_train = list(p.iterdir())
    # raw_h5 = [f for f in raw_files_train if "j0126_gt_v_" in str(f)]
    # labels_gt = list(p2.iterdir())
    
    
    # train_syn = KnossosRawDataSegm(data_path_x=raw_h5[27:57],
    #                               gt_path_x=labels_gt,
    #                               patch_shape=args.patch_shape,  # [z]yx
    #                               transform=train_transform,
    #                               bounds=tuple2bounds(args.train_bounds),  # xyz
    #                               epoch_size=args.epoch_size_train,
    #                               disable_memory_check=False,
    #                               verbose=args.verbose)
    
    # valid_dataset = KnossosRawDataSegm(data_path_x=raw_h5[2000:2900],
    #                               gt_path_x=labels_gt,
    #                               patch_shape=args.patch_shape,  # [z]yx
    #                               transform=valid_transform,
    #                               bounds=tuple2bounds(args.val_bounds),  # xyz
    #                               epoch_size=args.epoch_size_val,
    #                               disable_memory_check=False,
    #                               verbose=args.verbose)
    
    
    train_syn= KnossosRawDataSynthetic(data_path_x=args.data_path_x,
                                           gt_path_x=args.ground_truth_x,
                                           data_path_y=None,
                                           gt_path_y=None,
                                           data_path_z=None,
                                           gt_path_z=None, 
                                           patch_shape=args.patch_shape,  # [z]yx
                                           transform=train_transform,
                                           bounds=tuple2bounds([25,25,25,455,455,455]),  # xyz
                                           epoch_size=args.epoch_size_train,
                                           disable_memory_check=False,
                                           verbose=args.verbose)
    
    data_path_train = Path(args.data_path_z)
    raw_files_train = list(data_path_train.iterdir())
    raw_h5 = [f for f in raw_files_train if str(f).endswith(".h5")]
    raw_h5_train = raw_h5 
    
    gt_path_train = Path(args.ground_truth_z)
    unet_labels_train = list(gt_path_train.iterdir())
    u_labels_train = unet_labels_train
    print(f"len train = {len(u_labels_train)}")
    
    train_real= KnossosRawData(data_path_x=raw_h5_train,
                                           gt_path_x=u_labels_train,
                                           patch_shape=args.patch_shape,  # [z]yx
                                           transform=train_transform,
                                           bounds=tuple2bounds(args.train_bounds),  # xyz
                                           epoch_size=args.epoch_size_train,
                                           disable_memory_check=False,
                                           verbose=args.verbose)
    
    data_path_val = Path(args.data_path_z2)
    raw_files_val = list(data_path_val.iterdir())
    raw_h5 = [f for f in raw_files_val if str(f).endswith(".h5")]
    raw_h5_val = raw_h5
    print(raw_h5_val)
    
    gt_path_val = Path(args.ground_truth_z2)
    unet_labels_val = list(gt_path_val.iterdir())
    u_labels_val = unet_labels_val
    
    valid_real = KnossosRawData(data_path_x=raw_h5_val,
                                            gt_path_x=u_labels_val,
                                            # patch_shape=[30, 30, 30],  # [z]yx
                                            patch_shape=args.patch_shape,
                                            transform=valid_transform,
                                            bounds=tuple2bounds(args.val_bounds),  # xyz
                                            epoch_size=args.epoch_size_val,
                                            disable_memory_check=False,
                                            verbose=args.verbose)
    
    train_dataset = ConcatDataset(train_real, train_syn)
    # print(train_dataset[0])
    # train_dataset = train_syn
    valid_dataset = valid_real
    
    return train_dataset, valid_dataset    