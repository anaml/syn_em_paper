import os
from datetime import datetime

from parsing import parse_args
from dataloading import UNETDataConfig, setup_data 
from unet_helper import get_optimizer_class, get_criterion
from elektronn3.models.unet import UNet
from trainer import Trainer
# from elektronn3.training.trainer import Trainer
from modelloading import UNETModelConfig, setup_model
from unet_helper import _tb_log_sample_images_all_img, iou, dice_coefficient, accuracy, Evaluator
import torch
from pathlib import Path
from torchsummary import summary


def train():
    torch.cuda.empty_cache()
    args = parse_args()
    if args.experiment_name:
        exp_name = args.experiment_name
    else:
        exp_name = datetime.now().strftime('%y-%m-%d_%H-%M-%S-%f')
    save_path =os.path.expanduser(args.save_path)
    save_dir = os.path.join(save_path, exp_name + '/')
    tb_path = os.path.join(save_path, "tb_logs/")
    os.makedirs(save_path, exist_ok=True)
    print(f"save dir: {save_dir}")
    # set up data
    unet_data_config = UNETDataConfig(**vars(args))
    train_dataset, valid_dataset = setup_data(unet_data_config)
    unet_model_config = UNETModelConfig(**vars(args))
    model = setup_model(unet_model_config)

    ### next lines are for finetuning; just remove them for training from scratch
    # model_str = '23-05-08_21-03-30-073662' # miki_diff
    # model_str = '23-02-27_12-28-50-226393' # ana_v2_diff
    # model_path = Path(f'/cajal/nvmescratch/users/anaml/UNet_runs/{model_str}/state_dict_final.pth')
    # state_dict = torch.load(model_path)
    # state_dict = state_dict['model_state_dict']
    # model.load_state_dict(state_dict)
    ### end finetuning lines
    
    criterion = get_criterion(criterion_name=args.loss, 
                              dice_weight=args.dice_loss_weight, 
                              combined_weight=args.combined_loss_weight)
    optimizer_class = get_optimizer_class(optimizer_name=args.optimizer)
    optimizer = optimizer_class(list(model.parameters()), 
                                args.learning_rate, 
                                weight_decay=args.weight_decay)
    valid_metrics = {'accuracy': Evaluator(accuracy),
                     'iou': Evaluator(iou),
                     'dice_coefficient': Evaluator(dice_coefficient)
                    }
    trainer = Trainer(model=model, 
                      criterion=criterion, 
                      optimizer=optimizer,
                      device='cuda:0', 
                      save_root=save_path, 
                      train_dataset=train_dataset,
                      valid_dataset=valid_dataset,
                      valid_metrics=valid_metrics, 
                      preview_batch=None,
                      preview_interval=5,
                      inference_kwargs=None,
                      extra_save_steps=(),
                      exp_name=exp_name,
                      batch_size = args.batch_size, 
                      num_workers= args.num_workers,
                      overlay_alpha=0.0,
                      enable_tensorboard=True,
                      tensorboard_root_path=tb_path,
                      ipython_shell=False,
                      out_channels=args.out_channels,
                      sample_plotting_handler=_tb_log_sample_images_all_img,
                      preview_plotting_handler=None,
                      tqdm_kwargs=None
                      )
    trainer.run(args.max_steps)
    
    with open(f'{str(save_dir)}/hparams.txt', 'w') as f:
        f.write(str(args))
    