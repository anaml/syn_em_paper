import os
from datetime import datetime
import torch
from parsing import parse_args
from elektronn3.models.unet import UNet
from modelloading import UNETModelConfig, setup_model
# from el3_trainer import Trainer
from elektronn3.inference import Predictor
from pathlib import Path
import h5py
import numpy as np

model_str = '23-04-28_20-12-31-225510'
version = 'v1'
nr = '225510'

def inference():
    args = parse_args()
    
    # model_path = Path("/cajal/nvmescratch/users/anaml/UNet_runs/23-02-03_17-49-28-581342/state_dict_best.pth") 
    # model_path = Path('/u/anaml/UNet/runs/23-01-30_12-05-24-131102/state_dict_best.pth')
    model_path = Path(f'/cajal/nvmescratch/users/anaml/UNet_runs_diff/{model_str}/state_dict_final.pth')

    # model_path = Path("/cajal/nvmescratch/users/anaml/UNet_runs/23-02-08_16-38-04-745011/state_dict_best.pth")    
    p_syn_raw = Path('/cajal/nvmescratch/users/riegerfr/miki_seg/from_h5_em_cfw_1.0_full_3000_ana.npy') # 860419
    inference_path = Path(f'/cajal/scratch/users/anaml/3k_65000n_v3_diffusion/unet_inference/semseg_{version}_{nr}.hdf5') # 860419
    inference_path2 = Path(f'/cajal/scratch/users/anaml/3k_65000n_v3_diffusion/unet_inference/semseg_{version}_{nr}_raw_pred.hdf5') # 860419
    print(inference_path)
    unet_model_config = UNETModelConfig(**vars(args))
    model = setup_model(unet_model_config)
    state_dict = torch.load(model_path)
    state_dict = state_dict['model_state_dict']
    model.load_state_dict(state_dict)
    device = torch.device("cuda:0")
    model.to(device)
    
    # hf = h5py.File(str(p_syn_raw), "r")
    # cube_raw = hf['label_values'][()]
    cube_raw = np.load(str(p_syn_raw))[0, 0]
    print(f"Loaded syn data, shape = {cube_raw.shape}")
    print(f"max x:{cube_raw.max()}")
    print(f"min x:{cube_raw.min()}")
    # cube_raw = np.pad(cube_raw, 30, mode='constant')
    # print("Padded syn data to reduce artifacts at the boundaries")
    # print(f"Padded shape: {cube_raw.shape}")
    x = cube_raw / 255
    print(f"x:{x}")
    print(f"x.shape:{x.shape}")
    print(f"type x: {type(x)}")
    print(f"dtype x:{x.dtype}")
    x = x.astype(np.float32)[None]
    x = x.astype(np.float32)[None]
    x = torch.Tensor(x)
    print(f"x:{x}")
    print(f"x.shape:{x.shape}")
    print(f"type x: {type(x)}")
    print(f"dtype x:{x.dtype}")
    print(f"max x:{x.max()}")
    print(f"min x:{x.min()}")
    
    predictor = Predictor(model=model, state_dict_src=None, device="cuda:0", batch_size=1,
                          tile_shape=(100,100,100), overlap_shape=(30,30,30), offset=None, out_shape = (1,3000,3000,3000),
                          out_dtype=torch.float32, float16=False, apply_softmax=False, transform=None,
                          augmentations=None, strict_shapes=False, apply_argmax=False, argmax_with_threshold=None,
                          verbose=True, report_inp_stats=False)
    
    print("Initialized predictor for inference")
    
    inf_res = predictor.predict(x)
    sigmoid = torch.nn.Sigmoid()
    inf_res = sigmoid(inf_res)
    inf_res = inf_res.numpy()
    print(f"inf_res.shape:{inf_res.shape}")
    inference_res = inf_res[0,0]
    print(f"inference_res.shape:{inference_res.shape}")
    
    with h5py.File(str(inference_path2), 'w') as f:
        f.create_dataset("label_values", data=inference_res, dtype=np.float32)
        f.close()
        
    inference_res[inference_res >= 0.5] = 1
    inference_res[inference_res < 0.5] = 0
    inference_res = inference_res.astype(np.uint8)
    
    with h5py.File(str(inference_path), 'w') as f:
        f.create_dataset("label_values", data=inference_res, dtype=np.uint8)
        f.close()

if __name__ == '__main__':
    inference()
