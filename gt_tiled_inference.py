import os
from datetime import datetime
import torch
from parsing import parse_args
from elektronn3.models.unet import UNet
from modelloading import UNETModelConfig, setup_model
from elektronn3.inference import Predictor
from pathlib import Path
import h5py
import numpy as np
import scipy.ndimage as ndimage

m = '23-05-09_14-22-45-312157'
batch = 'test'

def inference():
    args = parse_args()
    model_path = Path(f'/cajal/nvmescratch/users/anaml/UNet_runs_diff/{m}/state_dict_better_1.591147099633897_20400.pth')
    
    path = Path(f'/cajal/scratch/users/anaml/GT/raw/{batch}')
    inference_path = Path(f'/cajal/scratch/users/anaml/GT/tiled_inference_unet/{m}/{batch}')
    os.makedirs(inference_path, exist_ok=True)
    print(inference_path)
    unet_model_config = UNETModelConfig(**vars(args))
    model = setup_model(unet_model_config)
    state_dict = torch.load(model_path)
    state_dict = state_dict['model_state_dict']
    model.load_state_dict(state_dict)
    device = torch.device("cuda:0")
    model.to(device)
    
    predictor = Predictor(model=model, state_dict_src=None, device="cuda:0", batch_size=1,
                          tile_shape=(50,50,50), overlap_shape=(30,30,30), offset=None, out_shape = (1,150,150,150),
                          out_dtype=torch.float32, float16=False, apply_softmax=False, transform=None,
                          augmentations=None, strict_shapes=False, apply_argmax=False, argmax_with_threshold=None,
                          verbose=True, report_inp_stats=False)
    
    print("Initialized predictor for inference")
    
    files = list(path.iterdir())
    print(f"Files={files}")
    for file in files:
        fp = str(file)
        if fp.endswith(".h5"):
            f = h5py.File(fp, 'r')
            x = f['em_raw'][()]
            x = x[256:406, 256:406, 256:406]
            # x = ndimage.zoom(x, zoom=[1, 1, 2])
            print("Loaded cube")
            print(f"Shape: {x.shape}")
            x = ((x * 2.0 )/ 255) - 1.0
            # x = x / 255
            # print(f"x:{x}")
            print(f"x.shape:{x.shape}")
            print(f"type x: {type(x)}")
            print(f"dtype x:{x.dtype}")
            x = x.astype(np.float32)[None]
            x = x.astype(np.float32)[None]
            x = torch.Tensor(x)
            # print(f"x:{x}")
            print(f"x.shape:{x.shape}")
            print(f"type x: {type(x)}")
            print(f"dtype x:{x.dtype}")
            print(f"max x:{x.max()}")
            print(f"min x:{x.min()}")
            sp = os.path.join(str(inference_path), fp.split('/')[-1])
            print(f"sp={sp}")
            inf_res = predictor.predict(x)
            sigmoid = torch.nn.Sigmoid()
            inf_res = sigmoid(inf_res)
            inf_res = inf_res.numpy()
            print(f"inf_res.shape:{inf_res.shape}")
            inference_res = inf_res[0,0]
            print(f"inference_res.shape:{inference_res.shape}")
            inference_res[inference_res >= 0.5] = 1
            inference_res[inference_res < 0.5] = 0
            inference_res = inference_res.astype(np.uint8)
    
            with h5py.File(str(sp), 'w') as h:
                h.create_dataset("label_values", data=inference_res, dtype=np.uint8)
                h.close()
    
 

if __name__ == '__main__':
    inference()
