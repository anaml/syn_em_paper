import os
import random
from typing import Callable, Dict, Optional, Sequence
import h5py
import numpy as np
import torch
import torch.utils.data
import scipy.ndimage as ndimage

from elektronn3.data import transforms


class KnossosRawData(torch.utils.data.Dataset):
    """Delivers raw patches that are randomly sampled from a KNOSSOS dataset.
    Supports 2D and 3D (choice is determined from the length of the
    ``patch_shape`` param).

    Args:
        conf_path: Path to KNOSSOS .conf file
        patch_shape: Shape (zyx) of patches that should be sampled from the
            dataset.
        transform: Transformation to be applied to the loaded data.
            See :py:mod:`elektronn3.data.transforms`.
        bounds: Tuple of boundary coordinates (xyz) that constrain the region
            of where patches should be sampled from within the KNOSSOS dataset.
            E.g. ``bounds=((256, 256, 128), (512, 512, 512))`` means that only
            the region between the low corner (x=256, y=256, z=128) and the
            high corner (x=512, y=512, z=512) of the dataset is considered. If
            ``None``, the whole dataset is used.
        mag: KNOSSOS magnification number
        mode: One of ``in_memory``, ``caching`` or ``disk``. If ``in_memory`` (default), the dataset (or the subregion
            that is constrained by ``bounds``) is pre-loaded into memory on initialization. If ``caching``, cache data
            from the disk and reuse it. If ``disk``, load data from disk on demand.
        epoch_size: Determines the length (``__len__``) of the ``Dataset``
            iterator. ``epoch_size`` can be set to an arbitrary value and
            doesn't have any effect on the content of produced training
            samples.
        disable_memory_check: If ``False`` (default), the amount of required
            memory is compared to the amount of free memory. If a planned
            allocation that exceeds 90% of free memory is detected, an error
            is raised. If ``True``, this check is disabled.
        verbose: If ``True``, be verbose about disk I/O.
        cache_size: If mode=``cache``: How many samples to hold in cache.
        cache_reuses: If mode=``cache``: How often to reuse a sample in cache before loading a new one from disk.
    """

    def __init__(
            self,
            data_path_x: str,
            gt_path_x: str,
            patch_shape: Sequence[int],  # [z]yx
            bounds: Sequence[Sequence[int]],  # xyz
            transform: Callable = transforms.Identity(),
            epoch_size: int = 100,
            disable_memory_check: bool = False,
            verbose: bool = False
    ):
        self.data_path_x = data_path_x
        self.gt_path_x = gt_path_x
        self.patch_shape_xyz = np.array(patch_shape)
        self.transform = transform
        self.epoch_size = epoch_size
        self.disable_memory_check = disable_memory_check
        self.verbose = verbose
        
        self.len_data = len(self.data_path_x)
        self.indices = list(np.zeros(self.len_data))
        # self.data= h5py.File(self.data_path_x, "r")
        # self.gt_file= h5py.File(self.gt_path_x, "r")
      

        self.dim = len(self.patch_shape_xyz)
        if self.dim == 2:
            self.patch_shape_xyz = np.array([*self.patch_shape_xyz, 1])  # z=1 for 2D
        if bounds is None:
            print("Bounds shouldn't be None")
            exit(0)
        self.bounds = np.array(bounds)
        self.shape = self.bounds[1] - self.bounds[0]
        print(f"self.shape:{self.shape}")
        # self.raw = self.data['label_values'][()]   # Will be filled with raw data if in_memory is True
        # self.gt = self.gt_file['label_values'][()]
        
        # self.data.close()
        # self.gt_file.close()
        
        self._load_into_memory()


    def __getitem__(self, index: int) -> Dict[str, torch.Tensor]:
        inp, gt, offset = self._load_from_memory()
        if self.dim == 2:
            inp = inp[0]  # squeeze z=1 dim -> yx
        inp = inp.astype(np.float32)[None]  # Prepend C dim -> (C, [D,] H, W)
        gt = gt.astype(np.float32)[None]
        if self.transform:
            inp, gt = self.transform(inp, gt)
            # print(f"inp.max:{inp.max()}")
            # print(f"inp.min:{inp.min()}")
        sample = {
            'inp': torch.as_tensor(inp),
            'target': torch.as_tensor(gt),
            'offset': offset
        }
        return sample
   
    def _load_from_memory(self) -> (np.ndarray, np.ndarray):
        # min_offset = (0, 0, 0)  # 0 because self.raw already accounts for min offset
        index = self.indices.index(0)
        
        if index == self.len_data - 1:
            self.indices = list(np.zeros(self.len_data))
        else:
            self.indices[index] = 1
                
        file_raw = self.data_path_x[index]
        file_labels = self.gt_path_x[index]    
        self.data= h5py.File(str(file_raw), "r")
        self.gt_file= h5py.File(str(file_labels), "r")
        self.raw = self.data['em_raw'][()]
        self.raw = self.raw[256:406, 256:406, 256:406]
        # self.raw = ndimage.zoom(self.raw, zoom=[1, 1, 2])
        self.raw = ((self.raw * 2.0 )/ 255) - 1.0
        self.gt = self.gt_file['label_values'][()]
        # self.gt = ndimage.zoom(self.gt, zoom=[1, 1, 2])
        self.data.close()
        self.gt_file.close()
        min_offset = self.bounds[0]
        # max_offset = self.shape - self.patch_shape_xyz
        max_offset = self.bounds[1] - self.patch_shape_xyz
        offset = np.random.randint(min_offset, max_offset + 1)  # xyz
        
        inp = self.raw[ 
                offset[0]:offset[0] + self.patch_shape_xyz[0],
                offset[1]:offset[1] + self.patch_shape_xyz[1],
                offset[2]:offset[2] + self.patch_shape_xyz[2],
        ]  # zyx (D, H, W)
           
        gt = self.gt[  
                offset[0]:offset[0] + self.patch_shape_xyz[0],
                offset[1]:offset[1] + self.patch_shape_xyz[1],
                offset[2]:offset[2] + self.patch_shape_xyz[2],
        ]  # zyx (D, H, W)
              
        return inp, gt, offset

    def __len__(self) -> int:
        return self.epoch_size

    def _load_into_memory(self) -> None:
        if not self.disable_memory_check:
            self.memory_check()
        if self.verbose:
            print('Loading dataset into memory...')


    def memory_check(self) -> None:
        # Memory cost in GiB: Number of gibivoxels * 4 because each uint8 voxel needs 1 byte.
        required_mem_gib = np.prod(self.bounds[1] - self.bounds[0]) / 1024 ** 3 * 1
        # Total RAM on the system in GiB
        free_mem_gib = float(os.popen('free -tm').readlines()[-1].split()[3]) / 1024
        mem_frac = required_mem_gib / free_mem_gib
        if mem_frac > 0.9:
            raise RuntimeError(
                f'Using in_memory and bounds of {self.bounds.tolist()} would require at least '
                f'{required_mem_gib:.2f} GiB of memory for each dataset instance.\n'
                'Please specifiy smaller bounds, or if you are sure about what you are '
                'doing, you can disable this check with the disable_memory_check flag.'
            )
