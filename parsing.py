import argparse
from argparse import ArgumentParser
from constants import BOUNDS_SYNTHETIC, BOUNDS_SYNTHETIC_SMALL, BOUNDS_SYNTHETIC_TRAIN, BOUNDS_SYNTHETIC_VAL, BOUNDS_J0126,BOUNDS_DIFFUSION_TRAIN, BOUNDS_DIFFUSION_VAL, BOUNDS_DIFFUSION_TRAIN_MIKI, BOUNDS_DIFFUSION_VAL_MIKI


def parse_args():
    parser: ArgumentParser = argparse.ArgumentParser(description='Train a 3D UNet.')

    parser.add_argument(
        '-dx', '--data-path-x', metavar='PATH',
        # default='/cajal/scratch/users/anaml/3k_50000n/tiled_inference_287463/syn_data_smallinp_287463_v2.hdf5',
        # default='/cajal/scratch/users/anaml/3k_50000n/tiled_inference_287463/syn_data_22-12-15_12-03-28-287463_v2.hdf5',
        # default='/cajal/scratch/users/anaml/3k_50000n/tiled_inference_860419/v2/syn_data_22-12-14_13-34-27-860419_v2.hdf5',
        # default='/cajal/scratch/users/anaml/3k_65000n/tiled_inference_957814/v3/syn_data_22-12-14_15-38-24-957814_v3.hdf5',
        # default='/cajal/scratch/users/anaml/GT/raw/batch3',
        # default='/cajal/nvmescratch/users/riegerfr/miki_seg/from_h5_em_cfw_1.0_full_3000_ana_v2.npy',
        # default='/cajal/nvmescratch/users/riegerfr/miki_seg/from_h5_em_long_train_cfw_1.0_cut1000_miki.npy',
        default='/cajal/nvmescratch/users/riegerfr/miki_seg/watershed_j0126_seg.npy',
        help='Path to the synthetic EM dataset(train)'
    )
    parser.add_argument(
        '-gtx', '--ground-truth-x', metavar='PATH',
        # default='/cajal/scratch/users/anaml/3k_50000n/unet_labels/labels_smallinp_287463.hdf5',
        # default='/cajal/scratch/users/anaml/3k_50000n/unet_labels/labels_860419_v2.hdf5',
        # default='/cajal/scratch/users/anaml/3k_65000n/unet_labels/labels_957814_v2.hdf5',
        # default='/cajal/scratch/users/anaml/GT/unet_labels/batch3',
        # default='/cajal/scratch/users/anaml/3k_65000n_v2/unet_labels/labels_957814_v1.hdf5',
        # default='/cajal/nvmescratch/users/anaml/miki/unet_labels/labels.hdf5',
        default='/cajal/nvmescratch/users/anaml/miki/unet_labels_3k/labels.hdf5',
        help='Path to the ground truth of the synthetic EM data(train)'
    )
    parser.add_argument(
        '-dy', '--data-path-y', metavar='PATH',
        # default='/cajal/scratch/users/anaml/3k_50000n/tiled_inference_287463/syn_data_smallinp_287463_v2.hdf5',
        # default='/cajal/scratch/users/anaml/3k_50000n/tiled_inference_860419/syn_data_22-12-14_13-34-27-860419_v1.hdf5',
        # default='/cajal/scratch/users/anaml/3k_65000n/tiled_inference_957814/syn_data_22-12-14_15-38-24-957814_v1.hdf5',
        # default='/cajal/scratch/users/anaml/3k_65000n_v2/tiled_inference_957814/syn_data_22-12-14_15-38-24-957814_v1.hdf5',  
        # default='/cajal/scratch/users/anaml/3k_50000n_v2/tiled_inference_860419/syn_data_22-12-14_13-34-27-860419_v1.hdf5',   
        # default='/cajal/scratch/users/anaml/GT/raw/validation', 
        # default='/cajal/nvmescratch/users/riegerfr/miki_seg/from_h5_em_cfw_1.0_full_3000_ana_v2.npy', 
        # default='/cajal/nvmescratch/users/riegerfr/miki_seg/from_h5_em_long_train_cfw_1.0_cut1000_miki.npy',     
        default='/cajal/nvmescratch/users/riegerfr/miki_seg/watershed_j0126_seg.npy',            
        help='Path to the synthetic EM dataset(validation)'
    )
    parser.add_argument(
        '-gty', '--ground-truth-y', metavar='PATH',
        # default='/cajal/scratch/users/anaml/3k_50000n/unet_labels/labels_smallinp_287463.hdf5',
        # default='/cajal/scratch/users/anaml/3k_50000n/unet_labels/labels_860419_v1.hdf5',
        # default='/cajal/scratch/users/anaml/3k_65000n/unet_labels/labels_957814_v1.hdf5',
        # default='/cajal/scratch/users/anaml/3k_65000n_v2/unet_labels/labels_957814_v1.hdf5',
        # default='/cajal/scratch/users/anaml/3k_50000n_v2/unet_labels/labels_860419_v1.hdf5',
        # default='/cajal/scratch/users/anaml/GT/unet_labels/validation', 
        # default='/cajal/scratch/users/anaml/3k_65000n_v2/unet_labels/labels_957814_v1.hdf5', 
        # default='/cajal/nvmescratch/users/anaml/miki/unet_labels/labels.hdf5',   
        default='/cajal/nvmescratch/users/anaml/miki/unet_labels_3k/labels.hdf5',   
        help='Path to the ground truth of the synthetic EM data(validation)'
    )
    parser.add_argument(
        '-dz', '--data-path-z', metavar='PATH',
        default='/cajal/scratch/users/anaml/GT/raw/batch3',
        help='Path to the synthetic EM dataset(train)'
    )
    parser.add_argument(
        '-gtz', '--ground-truth-z', metavar='PATH',
        default='/cajal/scratch/users/anaml/GT/unet_labels/batch3',
        help='Path to the ground truth of the synthetic EM data(val)'
    )
    parser.add_argument(
        '-dz2', '--data-path-z2', metavar='PATH',
        default='/cajal/scratch/users/anaml/GT/raw/validation', 
        help='Path to the synthetic EM dataset(train)'
    )
    parser.add_argument(
        '-gtz2', '--ground-truth-z2', metavar='PATH',
        default='/cajal/scratch/users/anaml/GT/unet_labels/validation', 
        help='Path to the ground truth of the synthetic EM data(val)'
    )
    parser.add_argument(
        '-me', '--max-epoch', type=int, default=400, # def 50
        help='Maximum epoch to train'
    )
    parser.add_argument(
        '-est', '--epoch-size-train', type=int, default=100, # def: 100
        help='How many training samples to process in a train epoch'
    )
    parser.add_argument(
        '-esv', '--epoch-size-val', type=int, default=50, # def: 10
        help='How many validation samples to process in a val epoch'
    )
    parser.add_argument(
        '-ms', '--max-steps', type=int, default=400*100, # def: 50*100=5000; max epoch * size train
        help='Max steps for training (nr epochs * len train dataset)'
    )
    parser.add_argument(
        '-sp', '--save-path', metavar='PATH', default='/cajal/nvmescratch/users/anaml/UNet_runs_diff_miki/',
        help='Path to save the model at'
    )
    parser.add_argument(
        '-en', '--experiment-name', default=None,
        help='Experiment name'
    )
    parser.add_argument(
        '-lr', '--learning-rate', type=float, default=0.001, #def 0.001
        help='Learning rate'
    )
    parser.add_argument(
        '-b', '--batch-size', type=int, default=2, 
        help='Batch size'
    )
    parser.add_argument(
        '-bl', '--batch-size-logging', type=int, default=4,
        help='Batch size for logging the constant samples'
    )
    parser.add_argument(
        '-se', '--seed', type=int, default=0,
        help='Seed for RNGs'
    )
    parser.add_argument(
        '-nc', '--number-classes', type=int, default=1,
        help='Number of classes: foreground and background'
    )
    parser.add_argument(
        '-ic', '--in-channels', type=int, default=1,
        help='Number of input channels'
    )
    parser.add_argument(
        '-oc', '--out-channels', type=int, default=1,
        help='Number of output channels. Depends on number of classes'
    )
    parser.add_argument(
        '-nb', '--n-blocks', type=int, default=4,
        help='Number of blocks'
    )
    parser.add_argument(
        '-sf', '--start-filts', type=int, default=64,
        help='Number of start filters'
    )
    parser.add_argument(
        '-um', '--up-mode', type=str, default='transpose',
        help='Upsampling mode'
    )
    parser.add_argument(
        '-mm', '--merge-mode', type=str, default='concat',
        help='Merge mode'
    )
    parser.add_argument(
        '-pb', '--planar-blocks', type=int, default=(),
        help='Number of input channels'
    )
    parser.add_argument(
        '-bn', '--batch-norm', type=str, default='unset',
        help=''
    )
    parser.add_argument(
        '-att', '--attention', type=bool, default=False,
        help=''
    )
    parser.add_argument(
        '-act', '--activation', nargs= '+', type=str, default='relu',
        help='Name of the non-linear activation. Choices: "relu", "silu", "leaky", "prelu", "rrelu" '
    )
    parser.add_argument(
        '-nn', '--normalization', type=str, default='batch',
        help=''
    )
    parser.add_argument(
        '-cm', '--conv-mode', type=str, default='same',
        help=''
    )
    parser.add_argument(
        '-ps', '--patch-shape', nargs='+', type=int, default=[100, 100, 100], # def: 100^3
        help='The shape of the patches which are sampled, xzy'
    )
    parser.add_argument(
        '-psl', '--patch-shape-logging', nargs='+', type=int, default=[64, 200, 200],
        help='The shape of the patches for tensorboard logging (and ground truth loss calc.) which are sampled, zxy'
    )
    parser.add_argument(
        '-l', '--loss', type=str, default="bce",
        help='Loss to be used. Choose between bce, dice and combined'
    )
    parser.add_argument(
        '-dlw', '--dice-loss-weight', type=float, default=1., 
        help='Weight for the dice loss. 0.5 weights both classes equally'
    )
    parser.add_argument(
        '-clw', '--combined-loss-weight', type=float, default=[0.5, 0.5], 
        help='Weight for the combined loss(bce and dice). 1. weights both losses equally'
    )
    parser.add_argument(
        '-nw', '--num-workers', type=int, default=10,
        help='how many subprocesses to use for data loading. 0 means that the data will be loaded in the main process'
    )
    parser.add_argument(
        '-vb', '--verbose', action='store_true',
        help='ensures logging/printing all statements'
    )
    parser.add_argument(
        '-mst', '--memory-stats', action='store_true',
        help='prints all memory statuses for all devices available'
    )
    parser.add_argument(
        '-tb', '--train-bounds', nargs='+', type=int, default=BOUNDS_J0126, #def: BOUNDS_SYNTHETIC
        help='Bounds for the train'
    )
    parser.add_argument(
        '-valb', '--val-bounds', nargs='+', type=int, default=BOUNDS_J0126, # def: BOUNDS_SYNTHETIC
        help='Bounds for the val'
    )
    parser.add_argument(
        '-bs', '--buffer-size', type=int, default=0, # def: 50
        help='Number of batches in the buffer for training. For deactivating, set to 0'
    )
    parser.add_argument(
        '-dp', '--dropout', type=float, default=0.0,
        help='dropout'
    )
    parser.add_argument(
        '-wd', '--weight-decay', type=float, default=1e-8,
        help='Weight decay for optimizers'
    )
    parser.add_argument(
        '-o', '--optimizer', type=str, default='Adam',
        help='optimizer'
    )
    parser.add_argument(
        '-pm', '--padding-mode', type=str, default="same", # def: valid
        help='padding mode for the generator models. One of "valid" or "same"'
    )
    parser.add_argument(
        '-fact', '--final-activation', type=str, default="sigmoid",
        help='what activation function to use in the last layer. One of "sigmoid", "tanh", '
             '"algebraic_sigmoid", "rescale" or "none". Make sure the value range is appropriate'
    )
    parser.add_argument(
        '-nl', '--normalization-layer', type=str, default="batchnorm", # def batchnorm
        help='what normalization layer to use. One of "batchnorm" or "groupnorm"'
    )
    parser.add_argument(
        '-gnng', '--groupnorm-num-groups', type=int, default=2,
        help='if normalization_layer="groupnorm", number of groups'
    )
   
    args = parser.parse_args()

    print(args)
    return args

