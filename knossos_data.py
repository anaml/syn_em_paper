import os
import random
from typing import Callable, Dict, Optional, Sequence
import h5py
import numpy as np
import torch
import torch.utils.data

from elektronn3.data import transforms


class KnossosRawDataSynthetic(torch.utils.data.Dataset):
    def __init__(
            self,
            data_path_x: str,
            gt_path_x: str,
            patch_shape: Sequence[int],  # [z]yx
            bounds: Sequence[Sequence[int]],  # xyz
            transform: Callable = transforms.Identity(),
            epoch_size: int = 100,
            disable_memory_check: bool = False,
            verbose: bool = False,
            data_path_y: str = None,
            gt_path_y: str = None,
            data_path_z: str = None,
            gt_path_z: str = None
    ):
        self.data_path_x = data_path_x
        self.gt_path_x = gt_path_x
        self.data_path_y = data_path_y
        self.gt_path_y = gt_path_y
        self.data_path_z = data_path_z
        self.gt_path_z = gt_path_z
        self.patch_shape_xyz = np.array(patch_shape)
        self.transform = transform
        self.epoch_size = epoch_size
        self.disable_memory_check = disable_memory_check
        self.verbose = verbose
        
        # self.data= h5py.File(self.data_path_x, "r")
        # self.data= np.load(self.data_path_x)[0, 0]
        # print(f"self.data shape = {self.data.shape}")
        self.gt_file= h5py.File(self.gt_path_x, "r")
        # if self.data_path_y:
        #     self.data_y= h5py.File(self.data_path_y, "r")
        #     self.gt_file_y= h5py.File(self.gt_path_y, "r")
        # else:
        #     self.data_y=None
        #     self.gt_file_y=None
                
        # if self.data_path_z:
        #     self.data_z= h5py.File(self.data_path_z, "r")
        #     self.gt_file_z= h5py.File(self.gt_path_z, "r")
        # else:
        #     self.data_z=None
        #     self.gt_file_z=None

        self.dim = len(self.patch_shape_xyz)
        if self.dim == 2:
            self.patch_shape_xyz = np.array([*self.patch_shape_xyz, 1])  # z=1 for 2D
        if bounds is None:
            print("Bounds shouldn't be None")
            exit(0)
        self.bounds = np.array(bounds)
        self.shape = self.bounds[1] - self.bounds[0]
        print(f"self.shape:{self.shape}")
        # self.raw = self.data['label_values'][()]   # Will be filled with raw data if in_memory is True
        self.raw = np.load(self.data_path_x)[0, 0]
        print(f"self.raw shape = {self.raw.shape}")
        self.gt = self.gt_file['label_values'][()]
        # self.raw = np.pad(self.raw, 30, mode='constant')
        # self.gt = np.pad(self.gt, 30, mode='constant')
        self.nr_max_dataset = 1
        # if self.data_y:
        #     self.raw_y = self.data_y['label_values'][()]   # Will be filled with raw data if in_memory is True
        #     self.gt_y = self.gt_file_y['label_values'][()]
        #     self.nr_max_dataset += 1
        #     # self.raw_y = np.pad(self.raw_y, 30, mode='constant')
        #     # self.gt_y = np.pad(self.gt_y, 30, mode='constant')
        #     self.data_y.close()
        #     self.gt_file_y.close()
        # if self.data_z:
        #     self.raw_z = self.data_z['label_values'][()]   # Will be filled with raw data if in_memory is True
        #     self.gt_z = self.gt_file_z['label_values'][()]
        #     self.nr_max_dataset += 1
        #     self.data_z.close()
        #     self.gt_file_z.close()
        
        # self.data.close()
        self.gt_file.close()
        
        self._load_into_memory()


    def __getitem__(self, index: int) -> Dict[str, torch.Tensor]:
        inp, gt, offset = self._load_from_memory()
        if self.dim == 2:
            inp = inp[0]  # squeeze z=1 dim -> yx
        inp = inp.astype(np.float32)[None]  # Prepend C dim -> (C, [D,] H, W)
        gt = gt.astype(np.float32)[None]
        if self.transform:
            inp, gt = self.transform(inp, gt)
            # print(f"inp.max:{inp.max()}")
            # print(f"inp.min:{inp.min()}")
        sample = {
            'inp': torch.as_tensor(inp),
            'target': torch.as_tensor(gt),
            'offset': offset
        }
        return sample
   
    def _load_from_memory(self) -> (np.ndarray, np.ndarray):
        # min_offset = (0, 0, 0)  # 0 because self.raw already accounts for min offset
        min_offset = self.bounds[0]
        # max_offset = self.shape - self.patch_shape_xyz
        max_offset = self.bounds[1] - self.patch_shape_xyz
        offset = np.random.randint(min_offset, max_offset + 1)  # xyz
        dataset = np.random.randint(0,self.nr_max_dataset)
        # print(f"dataset={dataset}")
            
        if dataset == 0:
            inp = self.raw[ 
                offset[0]:offset[0] + self.patch_shape_xyz[0],
                offset[1]:offset[1] + self.patch_shape_xyz[1],
                offset[2]:offset[2] + self.patch_shape_xyz[2],
            ]  # zyx (D, H, W)
           
            gt = self.gt[  
                offset[0]:offset[0] + self.patch_shape_xyz[0],
                offset[1]:offset[1] + self.patch_shape_xyz[1],
                offset[2]:offset[2] + self.patch_shape_xyz[2],
            ]  # zyx (D, H, W)
        elif dataset == 1:
            inp = self.raw_y[ 
                offset[0]:offset[0] + self.patch_shape_xyz[0],
                offset[1]:offset[1] + self.patch_shape_xyz[1],
                offset[2]:offset[2] + self.patch_shape_xyz[2],
            ]  # zyx (D, H, W)
        
            gt = self.gt_y[  
                offset[0]:offset[0] + self.patch_shape_xyz[0],
                offset[1]:offset[1] + self.patch_shape_xyz[1],
                offset[2]:offset[2] + self.patch_shape_xyz[2],
            ]  # zyx (D, H, W)
        elif dataset == 2:
            inp = self.raw_z[ 
                offset[0]:offset[0] + self.patch_shape_xyz[0],
                offset[1]:offset[1] + self.patch_shape_xyz[1],
                offset[2]:offset[2] + self.patch_shape_xyz[2],
            ]  # zyx (D, H, W)
                
            gt = self.gt_z[  
                offset[0]:offset[0] + self.patch_shape_xyz[0],
                offset[1]:offset[1] + self.patch_shape_xyz[1],
                offset[2]:offset[2] + self.patch_shape_xyz[2],
            ]  # zyx (D, H, W)   
        else:
            raise ValueError("Only works with 3 datasets")     
                
        return inp, gt, offset

    def __len__(self) -> int:
        return self.epoch_size

    def _load_into_memory(self) -> None:
        if not self.disable_memory_check:
            self.memory_check()
        if self.verbose:
            print('Loading dataset into memory...')


    def memory_check(self) -> None:
        # Memory cost in GiB: Number of gibivoxels * 4 because each uint8 voxel needs 1 byte.
        required_mem_gib = np.prod(self.bounds[1] - self.bounds[0]) / 1024 ** 3 * 1
        # Total RAM on the system in GiB
        free_mem_gib = float(os.popen('free -tm').readlines()[-1].split()[3]) / 1024
        mem_frac = required_mem_gib / free_mem_gib
        if mem_frac > 0.9:
            raise RuntimeError(
                f'Using in_memory and bounds of {self.bounds.tolist()} would require at least '
                f'{required_mem_gib:.2f} GiB of memory for each dataset instance.\n'
                'Please specifiy smaller bounds, or if you are sure about what you are '
                'doing, you can disable this check with the disable_memory_check flag.'
            )
