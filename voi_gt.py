import sys
import rand_voi
from pathlib import Path
import h5py
import json
import numpy as np
import os
import scipy.ndimage as ndimage



sys.path.insert(1, '/cajal/u/anaml/Thesis/evaluate')
batch = 'test'
model = '23-05-09_14-22-45-312157'
p_gen_instance_seg = Path(f'/cajal/scratch/users/anaml/GT/instance_segm/{model}/{batch}')
ids_path = Path(f'/cajal/scratch/users/anaml/GT/labels/{batch}')

files = list(ids_path.iterdir())
for file in files:
    fp = str(file)
    if fp.endswith(".h5"):
        print(fp)
        hf1 = h5py.File(str(fp), "r")
        ids = hf1['label_values'][()]  
        unique, counts = np.unique(ids, return_counts=True)
        print(f"Max IDs:{ids.max()}")
        print(f"so many instances GT: {len(unique)}")
        ids = ids[5:145, 5:145, 5:145]
        print(f"ids.shape:{ids.shape}")
        hf1.close()
        sp = os.path.join(str(p_gen_instance_seg), fp.split('/')[-1])
        hf_gen = h5py.File(str(sp), "r")
        gen = hf_gen['label_values'][()]
        unique, counts = np.unique(gen, return_counts=True)
        print(f"Max IDs:{gen.max()}")
        print(f"so many instances gen: {len(unique)}")
        print(f"gen.shape:{gen.shape}")
        hf_gen.close()
        print("Running VOI")
        metrics = rand_voi.rand_voi(ids, gen)
        print("Done")
        print(f"Metrics: {metrics}")
        print(f"Total VOI: {metrics['voi_split'] + metrics['voi_merge']}")

