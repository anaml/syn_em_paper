from elektronn3.models.unet import UNet
from typing import Union
from torch import nn

class UNETModelConfig:
    def __init__(self,
                 in_channels: int,
                 out_channels: int,
                 n_blocks: int,
                 start_filts: int,
                 up_mode: str,
                 merge_mode: str,
                 batch_norm: str,
                 attention: bool,
                 activation: Union[str, nn.Module],
                 normalization: str,
                 conv_mode: str,
                 **kwargs
                 ):
        """
        The configuration for the UNet 
        :param in_channels: Nr of input channels
        :param out_channels: Nr of output channels
        :param n_blocks: Number of downsampling/convolution blocks 
        :param start_filts: Number of filters for the first convolution layer
        :param up_mode: Upsampling method in the decoder pathway
        :param merge_mode: How the features from the encoder pathway should be combined with the decoder features
        :param attention: If ``True``, use grid attention in the decoder
        :param normalization:  Type of normalization that should be applied at the end of each block
        :param conv_mode: Padding mode of convolutions
        """
        self.in_channels = in_channels
        self.out_channels = out_channels
        self.n_blocks = n_blocks
        self.start_filts = start_filts
        self.up_mode = up_mode
        self.merge_mode = merge_mode
        self.batch_norm = batch_norm
        self.attention = attention
        self.activation = activation
        self.normalization = normalization
        self.conv_mode = conv_mode

def setup_model(args: UNETModelConfig):
    model = UNet(in_channels=args.in_channels, 
                 out_channels=args.out_channels, 
                 n_blocks=args.n_blocks,
                 start_filts=args.start_filts, 
                 up_mode=args.up_mode, 
                 merge_mode=args.merge_mode,
                 batch_norm=args.batch_norm, 
                 attention=args.attention, 
                 activation=args.activation,
                 normalization=args.normalization, 
                 conv_mode=args.conv_mode)
    
    return model        
