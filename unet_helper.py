import torch
from elektronn3.modules.loss import DiceLoss, CombinedLoss
from typing import Optional, Dict, Callable
import h5py
from pathlib import Path
import numpy as np
from elektronn3.training.handlers import plot_image
from functools import lru_cache

save_path = Path('/cajal/scratch/users/anaml/3k_50000n/check_predictions_3.hdf5')
eps = 0.0001  # To avoid divisions by zero

def _channelwise_sum(x: torch.Tensor):
    """Sum-reduce all dimensions of a tensor except dimension 1 (C)"""
    reduce_dims = tuple([0] + list(range(x.dim()))[2:])  # = (0, 2, 3, ...)
    return x.sum(dim=reduce_dims)

def dice_loss(probs, target, weight=1., eps=0.0001, smooth=0.):
    # Probs need to be softmax probabilities, not raw network outputs
    tsh, psh = target.shape, probs.shape

    if tsh == psh:  # Already one-hot
        onehot_target = target.to(probs.dtype)
    elif tsh[0] == psh[0] and tsh[1:] == psh[2:]:  # Assume dense target storage, convert to one-hot
        onehot_target = torch.zeros_like(probs)
        onehot_target.scatter_(1, target.unsqueeze(1), 1)
    else:
        raise ValueError(
            f'Target shape {target.shape} is not compatible with output shape {probs.shape}.'
        )
  
    intersection = probs * onehot_target  # (N, C, ...)
    numerator = 2 * _channelwise_sum(intersection) + smooth  # (C,)
    denominator = probs + onehot_target  # (N, C, ...)
    denominator = _channelwise_sum(denominator) + smooth + eps  # (C,)
    loss_per_channel = 1 - (numerator / denominator)  # (C,)
    weighted_loss_per_channel = weight * loss_per_channel  # (C,)
    return weighted_loss_per_channel.mean()  # ()



class DiceLoss(torch.nn.Module):
    def __init__(
            self,
            apply_sigmoid: bool = True,
            weight: Optional[torch.Tensor] = None,
            smooth: float = 0.
            ):
        
        super().__init__()
        if apply_sigmoid:
            self.sigmoid = torch.nn.Sigmoid()
        else:
            self.sigmoid = lambda x: x  # Identity (no sigmoid)
        self.dice = dice_loss
        if weight is None:
            weight = torch.tensor(1.)
        self.register_buffer('weight', weight)
        self.smooth = smooth

    def forward(self, output, target):
        probs = self.sigmoid(output)
        return self.dice(probs, target, weight=self.weight, smooth=self.smooth)


class SigmoidBCELoss(torch.nn.Module):
    def __init__(self, *args, **kwargs):
        super().__init__()
        self.bce = torch.nn.BCELoss(*args, **kwargs)

    def forward(self, output, target):
        # print(f"output max bf softmax: {output.max()}")
        # print(f"output min bf softmax: {output.min()}")
        # print(f"output shape: {output.shape}")
        sigmoid = torch.nn.Sigmoid()
        probs = sigmoid(output)
        # print(f"probs min: {probs.min()}")
        # print(f"probs max: {probs.max()}")
        # with h5py.File(str(save_path), 'w') as f:
        #     f.create_dataset("preds", data=probs.detach().cpu().numpy(), dtype=np.single)
        #     f.create_dataset("target", data=target.detach().cpu().numpy(), dtype=np.single)
        #     f.close()    
        # print(f"probs max: {probs.max()}")
        # print(f"probs min: {probs.min()}")
        # print("Saved preds and target tensors")
        # print(f"target max: {target.max()}")
        # print(f"target min: {target.min()}")
        loss = self.bce(probs, target)
        # print(f"loss:{loss}")
        if loss < 0: 
            loss = -loss
        # print(f"loss:{loss}")
        return loss

def get_optimizer_class(optimizer_name):
    if optimizer_name == 'Adam':
        optimizer_class = torch.optim.Adam  # set beta1 to 0.5? see:
        # https://www.kaggle.com/c/generative-dog-images/discussion/98993
        # or https://machinelearningmastery.com/how-to-train-stable-generative-adversarial-networks/
    elif optimizer_name == 'SGD':
        optimizer_class = torch.optim.SGD
    else:
        raise ValueError(f"optimizer must be either 'Adam' or 'SGD', not {optimizer_name}")
    return optimizer_class

def get_criterion(criterion_name, dice_weight=0.5, combined_weight=None):
    if criterion_name == 'combined':
        return CombinedLoss(criteria=[DiceLoss(apply_sigmoid=True, weight=torch.tensor(dice_weight)), SigmoidBCELoss()], 
                            weight=torch.tensor(combined_weight), 
                            device='cuda:0')
    elif criterion_name == "dice":
            return DiceLoss(apply_sigmoid=True, 
                            weight=torch.tensor(dice_weight))
    elif criterion_name == 'bce':
            return SigmoidBCELoss()
    else:
        raise ValueError(f"criterion must be either 'bce', 'dice' or 'combined',  not {criterion_name}")


def _tb_log_sample_images_all_img (
    trainer: 'Trainer',
    images: Dict[str, np.ndarray],
    z_plane: Optional[int] = None,
    group: str = 'sample'
    ) -> None:
    """Tensorboard plotting handler that plots all arrays in the ``images``
    dict as 2D grayscale images. Multi-channel images are split along the
    C dimension and plotted separately.
    """
    inp_batch = images['inp'][:1]
    # print(f"images[inp].shape:{images['inp'].shape}")
    # print(f"images[inp][:1].shape:{inp_batch.shape}")
    target_batch = images.get('target')
    # print(f"target_batch.shape:{target_batch.shape}")
    if target_batch is not None:
        target_batch = target_batch[:1]
    out_batch = images['out'][:1]
    # print(f"target_batch[:1].shape:{target_batch.shape}")
    # name = images.pop('fname', [None])[0]
    name = images.get('fname')
    if name is not None:
        name = name[0]
    
    out_batch = torch.sigmoid(torch.as_tensor(out_batch, dtype=torch.float32)).numpy()  
    # print(f"out_batch shape: {out_batch.shape}")  
    
    input = inp_batch[0,0,0]
    # print(f"input:{input.shape}")
    # print(f"inp_batch:{inp_batch.shape}") 
    # print(f"input min: {input.min()}")
    # print(f"input max: {input.max()}")   
    target = target_batch[0,0,0]
    # print(f"target:{target.shape}")
    # print(f"target_batch:{target_batch.shape}")
    # print(f"target min: {target.min()}")
    # print(f"target max: {target.max()}")   
    out = out_batch[0,0,0]
    # print(f"out:{out.shape}")
    # print(f"out_batch:{out_batch.shape}")
    # print(f"out min: {out.min()}")
    # print(f"out max: {out.max()}")   
    out[out>=0.5] = 1.
    out[out<0.5] = 0.
    
    trainer.tb.add_figure(
        f'{group}/inp',
        plot_image(input, cmap='gray', filename=name),
        global_step=trainer.step
    )
    
    trainer.tb.add_figure(
        f'{group}/target',
        plot_image(target, cmap='gray', filename=name),
        global_step=trainer.step
    )
     
    trainer.tb.add_figure(
        f'{group}/pred',
        plot_image(out, cmap='gray', filename=name),
        global_step=trainer.step
    )
    

@lru_cache(maxsize=128)
def confusion_matrix(
        target: torch.LongTensor,
        pred: torch.LongTensor,
        num_classes: int = 2,
        dtype: torch.dtype = torch.float32,
        device: torch.device = torch.device('cpu'),
        nan_when_empty: bool = True,
        ignore: Optional[int] = None,
) -> torch.Tensor:
    """ Calculate per-class confusion matrix.

    Uses an LRU cache, so subsequent calls with the same arguments are very
    cheap.

    Args:
        pred: Tensor with predicted class values
        target: Ground truth tensor with true class values
        num_classes: Number of classes that the target can assume.
            E.g. for binary classification, this is 2.
            Classes are expected to start at 0
        dtype: ``torch.dtype`` to be used for calculation and output.
            ``torch.float32`` is used as default because it is robust
            against overflows and can be used directly in true divisions
            without re-casting.
        device: PyTorch device on which to store the confusion matrix
        nan_when_empty: If ``True`` (default), the confusion matrix will
            be filled with NaN values for each channel of which there are
            no positive entries in the ``target`` tensor.
        ignore: Index to be ignored for cm calculation

    Returns:
        Confusion matrix ``cm``, with shape ``(num_classes, 4)``, where
        each row ``cm[c]`` contains (in this order) the count of
        - true positives
        - true negatives
        - false positives
        - false negatives
        of ``pred`` w.r.t. ``target`` and class ``c``.

        E.g. ``cm[1][2]`` contains the number of false positive predictions
        of class ``1``.
        If ``nan_when_empty`` is enabled and there are no positive elements
        of class ``1`` in ``target``, ``cm[1]`` will instead be filled with
        NaN values.
    """
    cm = torch.empty(num_classes, 4, dtype=dtype, device=device)
    for c in range(num_classes):
        # print(f"c: {c}")
        # print(f"num classes cm: {num_classes}")
        # print(f"pred max:{pred.max()}")
        # print(f"pred min:{pred.min()}")
        pos_pred = pred < 0.5
        neg_pred = pred >= 0.5
       
        pos_target = target == c
        
        if ignore is not None:
            ign_target = target == ignore
        else:
            ign_target = False  # Makes `& ~ign_target` a no-op
        # Manual conversion to Tensor because of a type promotion regression in PyTorch 1.5
        ign_target = torch.tensor(ign_target, dtype=torch.bool, device=device)
        neg_target = ~pos_target

        true_pos = (pos_pred & pos_target & ~ign_target).sum(dtype=dtype)
        true_neg = (neg_pred & neg_target & ~ign_target).sum(dtype=dtype)
        false_pos = (pos_pred & neg_target & ~ign_target).sum(dtype=dtype)
        false_neg = (neg_pred & pos_target & ~ign_target).sum(dtype=dtype)

        cm[c] = torch.tensor([true_pos, true_neg, false_pos, false_neg])

        if nan_when_empty and pos_target.sum(dtype=dtype) == 0:
            cm[c] = torch.tensor([float('nan')] * 4)

    return cm

def accuracy(target, pred, num_classes=2, mean=True, ignore=None):
    """Accuracy metric (in %)"""
    # print(f"num_classes acc:{num_classes}")
    cm = confusion_matrix(target, pred, num_classes=num_classes, ignore=ignore)
    tp, tn, fp, fn = cm.transpose(0, 1)  # Transposing to put class axis last
    acc = (tp + tn) / (tp + tn + fp + fn + eps)  # Per-class accuracy
    if mean:
        acc = acc.mean().item()
    return acc * 100



def dice_coefficient(target, pred, num_classes=2, mean=True, ignore=None):
    """Sørensen-Dice coefficient a.k.a. DSC a.k.a. F1 score (in %)"""
    # print(f"num_classes dice:{num_classes}")
    cm = confusion_matrix(target, pred, num_classes=num_classes, ignore=ignore)
    tp, tn, fp, fn = cm.transpose(0, 1)  # Transposing to put class axis last
    dsc = 2 * tp / (2 * tp + fp + fn + eps)  # Per-class (Sørensen-)Dice similarity coefficient
    if mean:
        dsc = dsc.mean().item()
    return dsc * 100



def iou(target, pred, num_classes=2, mean=True, ignore=None):
    """IoU (Intersection over Union) a.k.a. IU a.k.a. Jaccard index (in %)"""
    # print(f"num_classes iou:{num_classes}")
    cm = confusion_matrix(target, pred, num_classes=num_classes, ignore=ignore)
    tp, tn, fp, fn = cm.transpose(0, 1)  # Transposing to put class axis last
    iu = tp / (tp + fp + fn + eps)  # Per-class Intersection over Union
    if mean:
        iu = iu.mean().item()
    return iu * 100    
 
class Evaluator:
    name: str = 'generic'

    def __init__(
            self,
            metric_fn: Callable,
            index: Optional[int] = None,
            ignore: Optional[int] = None,
            self_supervised: Optional[bool] = False
    ):
        self.metric_fn = metric_fn
        self.index = index
        self.ignore = ignore
        self.num_classes = None
        self.self_supervised = self_supervised

    def __call__(self, target: torch.Tensor, out: torch.Tensor) -> float:
        # self supervised training 
        if self.self_supervised:
            m = self.metric_fn(target, out)
            return m  # aggregation handled by sklearn metric
        # supervised training
        else:
            if self.num_classes is None:
                self.num_classes = out.shape[1]
            # print(self.num_classes)
            sigmoid = torch.nn.Sigmoid()
            pred = sigmoid(out)
            m = self.metric_fn(target, pred, self.num_classes, mean=False, ignore=self.ignore)
            if self.index is None:
                return m.mean().item()
            return m[self.index].item()
 