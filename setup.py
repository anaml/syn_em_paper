from setuptools import setup
from setuptools.extension import Extension
from Cython.Build import cythonize
import numpy as np

setup(
        ext_modules=cythonize([
            Extension(
                'rand_voi',
                sources=[
                    '/cajal/u/anaml/UNet/rand_voi.pyx'
                ],
                extra_compile_args=['-O3', '-std=c++11'],
                include_dirs=[np.get_include()],
                language='c++')
        ])
)